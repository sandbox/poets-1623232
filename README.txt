The floor plan module helps you to create a custom map from an image.

It works very similar to an image map.
Users can have as many images as they need.

We recommend the following:

* Imagines have to be square (2000 x 2000 size)
* Create field under content type and describe the floor name
* Under default value, use Image Path/Name field for default image
* Create new content. Position of the identifier will be in the center
* Change coordinates, as you need in Location field
* Use Description field to describe what is in that Location/Position


Credits
-------

Providers Of Educational Technology Support (POETS), Graduate College of 
Education, San Francisco State University.

* Jonathan Foerster
* Carlos Romero-Julio
* Robert Demallac-Sauzier 

Maintainer (CRJ)
