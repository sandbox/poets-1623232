var flg_title = Drupal.settings.floor_plan.title;
var flg_location = Drupal.settings.floor_plan.location;
var flg_path = Drupal.settings.floor_plan.path;
var po = org.polymaps;

var map = po.map()
    .container(document.getElementById("inner").appendChild(po.svg("svg")))
    //.center({lat: 37.768, lon: -122.415})
    //.zoom(13)
    .zoomRange([12, 16])
    .add(po.interact());

map.add(po.layer(floor_plan)
    .tile(false));

map.add(po.compass()
    .pan("none"));

/** experiment CRJ
map.add(po.image()
    .url(po.url(flg_path)));
*/

/** Image loaded. */
function floor_plan(tile, proj) {
  proj = proj(tile);
  var tl = proj.locationPoint({lon: -122.500, lat: 37.815}),
      br = proj.locationPoint({lon: -122.320, lat: 37.715}),
      image = tile.element = po.svg("image");
  image.setAttribute("preserveAspectRatio", "none");
  image.setAttribute("x", tl.x);
  image.setAttribute("y", tl.y);
  image.setAttribute("width", br.x - tl.x);
  image.setAttribute("height", br.y - tl.y);
  image.setAttributeNS("http://www.w3.org/1999/xlink", "href", flg_path);
}

/** Location. */
var a = flg_location.split(",")

var fpl = {};
var base_lon = [-122.415]
    fp_lon = Number(base_lon) + (Number([a[0]]) * 0.001);
var base_lat = [37.768]
    fp_lat = Number(base_lat) + (Number([a[1]]) * 0.001);

var fp_c = [fp_lon].concat([fp_lat]);

var fpl = [{"geometry": {"coordinates": fp_c, "type": "Point"}}];

map.add(po.geoJson()
    .on("load", load)
    .features(fpl)
    );

/** Post-process the GeoJSON points */
function load(e) {
  var r = 20 * Math.pow(2, e.tile.zoom - 13);
  for (var i = 0; i < e.features.length; i++) {
    var c = n$(e.features[i].element),
        g = c.parent().add("svg:g", c);

    g.attr("transform", "translate(" + c.attr("cx") + "," + c.attr("cy") + ")");

    g.add(c)
      .attr("cx", null)
      .attr("cy", null)
      .attr("r", r)
      .attr("title", flg_title)
      .attr("alt", flg_title);
  }
}
